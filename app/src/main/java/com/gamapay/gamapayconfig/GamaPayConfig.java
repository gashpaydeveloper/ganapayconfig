package com.gamapay.gamapayconfig;

/**
 * Created by jackyang on 2021/2/4.
 */
public class GamaPayConfig {
    public GamaPayConfig() { }

    public static native String GamaPayIV();

    public static native String GamaPayKEY();

    public static native String[] GamaPayTestAccountList();

    static {
        System.loadLibrary("GamaPayConfig");
    }

}
