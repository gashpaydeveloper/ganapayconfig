package com.gamapay.gamapayconfig

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gamapay.gamapayconfig.GamaPayConfig.*
import kotlinx.android.synthetic.main.activity_gamapay_config.*

class GamaPayConfigActivity : AppCompatActivity() {
    private val GamaPayIV= GamaPayIV()
    private val GamaPayKEY= GamaPayKEY()
    private val GamaPayTestAccountList : String
        get() = run {
            var accountList = ""
            for (account in GamaPayTestAccountList()) {
                accountList += " $account"
            }
            return accountList
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gamapay_config)

        tv_config.text = String.format("%s\n\n%s\n\n%s",
            "GamaPayIV : $GamaPayIV",
            "GamaPayKEY : $GamaPayKEY",
            "GamaPayTestAccountList : $GamaPayTestAccountList"
        )
    }
}
