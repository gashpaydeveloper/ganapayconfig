#include <jni.h>
#include <string>

#define arraySize 8

/**
 * GamaPayIV
 */
extern "C" JNIEXPORT jstring JNICALL
Java_com_gamapay_gamapayconfig_GamaPayConfig_GamaPayIV(
        JNIEnv *env,
        jclass clazz) {
    std::string GamaPayIV = "OGChU2k5cK1u6CIaign8lw==";
    return env->NewStringUTF(GamaPayIV.c_str());
}

/**
 * GamaPayKEY
 */
extern "C" JNIEXPORT jstring JNICALL
Java_com_gamapay_gamapayconfig_GamaPayConfig_GamaPayKEY(
        JNIEnv *env,
        jclass clazz) {
    std::string GamaPayKEY = "fDpl/Hq2VivUUWg9+r6Zsu4W7QO9D6pqr2QX+0/6jC4=";
    return env->NewStringUTF(GamaPayKEY.c_str());

}

extern "C" JNIEXPORT jobjectArray JNICALL
Java_com_gamapay_gamapayconfig_GamaPayConfig_GamaPayTestAccountList
        (JNIEnv *env, jclass jobj) {
    jobjectArray ret;
    int i;

    const char *data[arraySize] = {"gamapay01", "gamapay02", "gamapay03", "gamapay04", "gamapay05", "gamapay06", "jack58", "test168"};
    ret = (jobjectArray) env->NewObjectArray(arraySize, env->FindClass("java/lang/String"),
                                             env->NewStringUTF(""));
    for (i = 0; i < arraySize; i++) env->SetObjectArrayElement(ret, i, env->NewStringUTF(data[i]));

    return (ret);
}
